<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use App\Repository\ProjectRepository;

use App\Entity\Project;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Service\FileService;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\JsonResponse;

class ProjectController extends Controller
{
    private $serializer;

    public function __construct()
    {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(1);
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });
        $this->serializer = new Serializer([$normalizer], [$encoder]);
    }

    /**
     * @Route("/project", name="all_project", methods={"GET"})
     */

    public function all(ProjectRepository $repo)
    {
        $list = $repo->findAll();

        $data = $this->serializer->normalize($list, null, ['attributes' => ['id', 'name', 'description', 'link', 'git', 'img']]);

        $response = new Response($this->serializer->serialize($data, "json"));

        return $response;
    }

    /**
     * @Route("/project/{project}", name="one_project", methods={"GET"})
     */

    public function one(Project $project)
    {
        $data = $this->serializer->normalize($project, null, ['attributes' => ['id', 'name', 'description', 'link', 'git', 'img']]);

        $response = new Response($this->serializer->serialize($data, "json"));
        return $response;
    }

    /**
     * @Route("/project", name="new_project", methods={"POST"})
     */

    public function add(Request $req, FileService $fileService)
    {   
        $image = $req->files->get("img");
        $absoluteUrl = $req->getScheme() . '://' . $req->getHttpHost() . $req->getBasePath();
        $imageUrl = $fileService->upload($image, $absoluteUrl);

        $project = new Project();
        $project->setDescription($req->get("description"));
        $project->setName($req->get("name"));
        $project->setGit($req->get("git"));
        $project->setLink($req->get("link"));
        $project->setImg($imageUrl);

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($project);
        $manager->flush();

        $json = $this->serializer->serialize($project, "json");

        return JsonResponse::fromJsonString($json);
    }


    /**
     * @Route("/project/{id}", name="updade_project", methods={"PUT"})
     */

    public function update(Request $request, Project $project)
    {
        $manager = $this->getDoctrine()->getManager();

        $content = $request->getContent();
        $image = $request->files->get("img");
        $absoluteUrl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();
        $imageUrl = $fileService->upload($image, $absoluteUrl);
        $update = $this->serializer->deserialize($content, Project::class, "json");

        $project->setDescription($request->get("description"));
        $project->setName($request->get("name"));
        $project->setGit($request->get("git"));
        $project->setLink($request->get("link"));
        $project->setImg($imageUrl);

        $manager->persist($project);
        $manager->flush();

        $data = $this->serializer->normalize($project, null, ['attributes' => ['id', 'name', 'description', 'link', 'git', 'img']]);

        $response = new Response($this->serializer->serialize($data, "json"));
        return $response;
    }

    /**
     * @Route("/project/{id}", name="delete_project", methods={"DELETE"})
     */

    public function delete(Project $project)
    {
        $manager = $this->getDoctrine()->getManager();

        $manager->remove($project);
        $manager->flush();

        $response = new Response("OK", 204);
        return $response;
    }
}
